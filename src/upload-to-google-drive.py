import os
import io
import boto3
from google.oauth2.credentials import Credentials
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError

# Replace the following variables with your own
s3_bucket_name = "migrpocbuckettriggerisetest"
s3_folder_names = ["capture", "verify"]
google_drive_folder_names = ["capture", "verify"]
google_drive_creds_parameter_name = "google-service-account-credentials"

# Set up the AWS client for S3 and Systems Manager
s3_client = boto3.client("s3")
ssm_client = boto3.client("ssm")


# Set up the Google Drive API client
def get_gdrive_service():
    creds_json_str = ssm_client.get_parameter(Name=google_drive_creds_parameter_name, WithDecryption=True)["Parameter"][
        "Value"]
    creds = Credentials.from_authorized_user_info(info={}, scopes=["https://www.googleapis.com/auth/drive"])
    service = build("drive", "v3", credentials=creds)
    return service


def check_folder_exists(service, folder_name):
    """
    Checks if a folder with the specified name exists in Google Drive.
    If it exists, returns the folder ID. If not, returns None.
    """
    query = "mimeType='application/vnd.google-apps.folder' and trashed=false and name='{}'".format(folder_name)
    results = service.files().list(q=query, fields="nextPageToken, files(id, name)").execute()
    items = results.get("files", [])
    if items:
        return items[0]["id"]
    else:
        return None


def create_folder(service, folder_name, parent_folder_id=None):
    """
    Creates a new folder with the specified name in Google Drive.
    If parent_folder_id is specified, the new folder will be created as a subfolder of the parent folder.
    Returns the ID of the newly created folder.
    """
    file_metadata = {
        "name": folder_name,
        "mimeType": "application/vnd.google-apps.folder"
    }
    if parent_folder_id:
        file_metadata["parents"] = [parent_folder_id]
    folder = service.files().create(body=file_metadata, fields="id").execute()
    return folder.get("id")


# Find or create the Google Drive folders
def find_or_create_gdrive_folders(service):
    folder_ids = {}
    for i in range(len(google_drive_folder_names)):
        folder_name = google_drive_folder_names[i]
        folder_id = None
        query = "mimeType='application/vnd.google-apps.folder' and name='%s' and trashed = false" % folder_name
        results = service.files().list(q=query, fields="nextPageToken, files(id, name)").execute()
        items = results.get("files", [])
        if not items:
            # Create the folder if it doesn't exist
            file_metadata = {
                "name": folder_name,
                "mimeType": "application/vnd.google-apps.folder"
            }
            folder = service.files().create(body=file_metadata, fields="id").execute()
            folder_id = folder.get("id")
        else:
            folder_id = items[0].get("id")
        folder_ids[folder_name] = folder_id
    return folder_ids


# Upload files from S3 to Google Drive
def upload_s3_files_to_gdrive(service, folder_ids):
    for i in range(len(s3_folder_names)):
        s3_folder_name = s3_folder_names[i]
        gdrive_folder_name = google_drive_folder_names[i]
        gdrive_folder_id = folder_ids[gdrive_folder_name]
        objects = s3_client.list_objects_v2(Bucket=s3_bucket_name, Prefix=s3_folder_name)["Contents"]
        for obj in objects:
            file_name = obj["Key"].split("/")[-1]
            file_content = s3_client.get_object(Bucket=s3_bucket_name, Key=obj["Key"])["Body"].read()
            file_metadata = {
                "name": file_name,
                "parents": [gdrive_folder_id]
            }
            media = io.BytesIO(file_content)
            try:
                file = service.files().create(body=file_metadata, media_body=media, fields="id").execute()
            except HttpError as error:
                print(f"An error occurred: {error}")
                file = None
            if file:
                print(f"File {file_name} uploaded to folder {gdrive_folder_name}")


def get_credentials():
    """
    Retrieves Google Drive credentials from AWS Parameter Store.
    """
    # Specify the name of the parameter that stores the Google Drive credentials
    parameter_name = "/my/google/drive/credentials"

    # Retrieve the parameter value from AWS Parameter Store
    ssm = boto3.client('ssm')
    parameter = ssm.get_parameter(Name=parameter_name, WithDecryption=True)

    # Parse the JSON string containing the credentials and create a Credentials object
    credentials_info = json.loads(parameter['Parameter']['Value'])
    credentials = Credentials.from_authorized_user_info(info=credentials_info)

    return credentials


# Main function
def main(event, context):
    # Authenticate and create the Drive API client
    creds = None
    try:
        creds = get_credentials()
    except Exception as e:
        print(f'Error getting credentials: {e}')
        return

    drive_service = build('drive', 'v3', credentials=creds)

    # Get the S3 client
    s3 = boto3.client('s3')

    # Get the names of the source and destination folders from the event
    src_folder = event['src_folder']
    dest_folder = event['dest_folder']

    # Check if the destination folder exists, and create it if it doesn't
    dest_folder_id = check_folder_exists(drive_service, dest_folder)
    if not dest_folder_id:
        dest_folder_id = create_folder(drive_service, dest_folder)

    # Get the objects in the source S3 folder
    objects = s3.list_objects_v2(Bucket=s3_bucket_name, Prefix=src_folder)
    if 'Contents' not in objects:
        print(f'No objects found in {src_folder}')
        return

    # Upload each object to the destination Drive folder
    for obj in objects['Contents']:
        obj_key = obj['Key']
        obj_name = os.path.basename(obj_key)
        obj_path = f'/tmp/{obj_name}'

        # Download the object from S3 to a temporary file
        s3.download_file(s3_bucket_name, obj_key, obj_path)

        # Upload the object to the destination Drive folder
        file_metadata = {'name': obj_name, 'parents': [dest_folder_id]}
        media = MediaFileUpload(obj_path)
        file = drive_service.files().create(body=file_metadata, media_body=media, fields='id').execute()
        print(f'Uploaded {obj_name} to Drive with ID: {file.get("id")}')

    print('Migration completed successfully')
