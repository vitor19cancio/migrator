#!/bin/bash

aws ssm put-parameter \
    --name "google-service-account-credentials" \
    --value "PLACEHOLDER_VALUE" \
    --type "SecureString" \
    --overwrite

echo "Parameter created successfully"