#!/bin/bash

# Set variables
stack_name="google-drive-lambda"
s3_bucket="migrpocbuckettriggerisetest"
google_credentials_parameter_name="google-service-account-credentials"

# Get account ID
aws_account_id=$(aws sts get-caller-identity --output text --query 'Account')

# Create CloudFormation stack
aws cloudformation create-stack \
    --stack-name "$stack_name" \
    --template-body file://lambda-resources.yaml \
    --capabilities CAPABILITY_AUTO_EXPAND \
    --parameters ParameterKey=S3BucketName,ParameterValue="$s3_bucket" \
                 ParameterKey=GoogleCredentialsParameterName,ParameterValue="$google_credentials_parameter_name" \
                 ParameterKey=AccountId,ParameterValue="$aws_account_id"
